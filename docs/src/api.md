# Internal

## Public APIs

```@autodocs
Modules = [TestParticle, TestParticleMakie]
Private = false
```

## Private types and methods

```@autodocs
Modules = [TestParticle]
Public = false
```
